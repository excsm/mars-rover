module CliSupport
  # Executes command, sends typed input
  # and captures and returns output of command
  def exec_cli(command, typed_input=[])
    output = []
    Open3.popen3(command, chdir: Mars::Rover.root) do |stdin, stdout, stderr, wait_thr|
      typed_input.each do |line|
        stdin.write("#{line}\n")
      end
      stdout.read.lines.each do |line|
        output << line.chomp
      end

      wait_thr.pid # pid of the started process.
      wait_thr.value # Process::Status object returned.
    end
    output.join("\n")
  end
end
