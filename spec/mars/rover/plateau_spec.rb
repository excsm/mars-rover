require 'spec_helper'

RSpec.describe Mars::Rover::Plateau do
  let(:plateau) { described_class.new(5, 5) }
  let(:rover) { Mars::Rover::Remote.new(plateau, [1, 2], Mars::Rover::NORTH) }
  let(:coordinates) { Mars::Rover::Coordinates.new([1, 3], Mars::Rover::NORTH) }

  describe '#place_rover' do
    subject { place_rover }

    let(:place_rover) { plateau.place_rover(rover, coordinates) }

    context 'when no rover' do
      let(:rover) { nil }

      it { is_expected.to be(nil) }
    end

    context 'when no coordinates' do
      let(:coordinates) { nil }

      it { is_expected.to be(nil) }
    end

    context 'when placing off the plateau' do
      let(:rover) { Mars::Rover::Remote.new(plateau, [1, 2], Mars::Rover::NORTH) }
      let(:coordinates) { Mars::Rover::Coordinates.new([1, 5], Mars::Rover::NORTH) }

      it do
        expect do
          place_rover
        end.to raise_error(Mars::Rover::TransmissionError, /out_of_bounds/)
      end
    end

    context 'when another rover in same position' do
      let(:other_rover) { Mars::Rover::Remote.new(plateau, [1, 3], Mars::Rover::EAST) }
      let(:other_coordinates) { Mars::Rover::Coordinates.new([1, 3], Mars::Rover::EAST) }

      before do
        plateau.place_rover(other_rover, other_coordinates)
      end

      it do
        expect do
          place_rover
        end.to raise_error(Mars::Rover::TransmissionError, /obstacle_conflict/)
      end
    end

    context 'when no obstacles for placement' do
      it do
        expect do
          place_rover
        end.not_to raise_error
      end
    end
  end

  describe '#rover_position' do
    context 'with rover placed' do
      subject { plateau.rover_position(rover) }

      before do
        plateau.place_rover(rover, coordinates)
      end

      it { is_expected.to be_an_instance_of(Mars::Rover::Coordinates) }
      its(:position) { is_expected.to eq([1, 3]) }
      its(:orientation) { is_expected.to be(Mars::Rover::NORTH) }
    end

    context 'with rover not placed' do
      subject { plateau.rover_position(rover) }

      it { is_expected.to be(nil) }
    end
  end
end
