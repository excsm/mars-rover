require 'spec_helper'

RSpec.describe Mars::Rover::Parser do
  subject(:parse) { parser.parse }

  let(:parser) { described_class.new(instruction_set) }

  context 'with no instruction set' do
    let(:instruction_set) { '' }
    it do
      expect { parse }
        .to raise_error(
          Mars::Rover::InstructionError, /Missing rover instructions!!!/
        )
    end
  end

  context 'with incomplete instruction set' do
    let(:instruction_set) { "5 5\n1 2 N" }
    it do
      expect { parse }
        .to raise_error(
          Mars::Rover::InstructionError, /Invalid rover instructions!!!/
        )
    end
  end

  context 'with invalid instruction set' do
    let(:instruction_set) { 'Troll time :troll_face:' }
    it do
      expect { parse }
        .to raise_error(
          Mars::Rover::InstructionError, /Invalid rover instructions!!!/
        )
    end
  end

  context 'with incomplete initial plateau size' do
    let(:instruction_set) { "5\nS 2 N\nLMLMLMLMM" }
    it do
      expect { parse }
        .to raise_error(
          Mars::Rover::InstructionError, /Invalid rover instructions!!!/
        )
    end
  end

  context 'with invalid initial plateau size' do
    let(:instruction_set) { "5 ?\nS 2 N\nLMLMLMLMM" }
    it do
      expect { parse }
        .to raise_error(
          Mars::Rover::InstructionError, /Invalid rover instructions!!!/
        )
    end
  end

  context 'with invalid initial rover x position' do
    let(:instruction_set) { "5 5\nS 2 N\nLMLMLMLMM" }
    it do
      expect { parse }
        .to raise_error(
          Mars::Rover::InstructionError, /Invalid rover instructions!!!/
        )
    end
  end

  context 'with invalid initial rover y position' do
    let(:instruction_set) { "5 5\n1 W N\nLMLMLMLMM" }
    it do
      expect { parse }
        .to raise_error(
          Mars::Rover::InstructionError, /Invalid rover instructions!!!/
        )
    end
  end

  context 'with invalid rover command' do
    let(:instruction_set) { "5 5\n1 W N\nLMLMFMLMM" }

    it do
      expect { parse }
        .to raise_error(
          Mars::Rover::InstructionError, /Invalid rover instructions!!!/
        )
    end
  end

  context 'with correct instruction set' do
    let(:instruction_set) { <<-INSTRUCTION_SET.strip }
        5 5
        1 2 N
        LMLMLMLMM
        3 3 E
        MMRMMRMRRM
        4 2 S
        MMRMRMMRMR
      INSTRUCTION_SET

    it { is_expected.to be_an_instance_of(Mars::Rover::InstructionSet) }

    context 'plateau size x' do
      subject { parser.parse.plateau_size_x }

      it { is_expected.to eq(5) }
    end

    context 'plateau size y' do
      subject { parser.parse.plateau_size_y }

      it { is_expected.to eq(5) }
    end

    context 'rover commands' do
      subject { parser.parse.instructions.size }

      it { is_expected.to be(3) }
    end

    context 'first rover initial position' do
      subject { parser.parse.instructions.first.position }

      it { is_expected.to eq([1,2]) }
    end

    context 'first rover initial orientation' do
      subject { parser.parse.instructions.first.orientation }

      it { is_expected.to eq(Mars::Rover::NORTH) }
    end

    context 'first rover movement commands' do
      subject { parser.parse.instructions.first.commands }

      let(:expected_commands) do
        [
          Mars::Rover::TURN_LEFT,
          Mars::Rover::MOVE_FORWARD,
          Mars::Rover::TURN_LEFT,
          Mars::Rover::MOVE_FORWARD,
          Mars::Rover::TURN_LEFT,
          Mars::Rover::MOVE_FORWARD,
          Mars::Rover::TURN_LEFT,
          Mars::Rover::MOVE_FORWARD,
          Mars::Rover::MOVE_FORWARD,
        ]
      end
      it { is_expected.to eq(expected_commands) }
    end
  end
end
