require 'spec_helper'

RSpec.describe Mars::Rover::Transmitter do
  subject(:transmit) { transmitter.transmit }

  let(:transmitter) { described_class.new(instruction_set) }

  context 'with no instruction set' do
    let(:instruction_set) { '' }

    it do
      expect { transmit }
        .to raise_error(
          Mars::Rover::InstructionError, /Missing rover instructions!!!/
        )
    end
  end

  context 'with incomplete instruction set' do
    let(:instruction_set) { "5 5\n1 2 N" }

    it do
      expect { transmit }
        .to raise_error(
          Mars::Rover::InstructionError, /Invalid rover instructions!!!/
        )
    end
  end

  context 'with invalid instruction set' do
    let(:instruction_set) { 'Troll time :troll_face:' }

    it do
      expect { transmit }
        .to raise_error(
          Mars::Rover::InstructionError, /Invalid rover instructions!!!/
        )
    end
  end

  context 'with correct instruction set' do
    subject { transmit }

    let(:transmission) { subject.print }

    context 'with one rover' do
      let(:instruction_set) { "5 5\n1 2 N\nLMLMLMLMM" }

      it { is_expected.to be_an_instance_of(Mars::Rover::Transmission) }
      it { expect(transmission).to eq('1 3 N') }
    end

    context 'with two rovers' do
      let(:instruction_set) { "5 5\n1 2 N\nLMLMLMLMM\n2 3 E\nMMRMMRMRRM" }

      it { is_expected.to be_an_instance_of(Mars::Rover::Transmission) }
      it { expect(transmission).to eq("1 3 N\n4 1 E") }
    end

    context 'with three rovers' do
      let(:instruction_set) { <<-INSTRUCTION_SET.strip }
          5 5
          1 2 N
          LMLMLMLMM
          2 3 E
          MMRMMRMRRM
          4 2 W
          MMRMRMMRMR
        INSTRUCTION_SET

      it { is_expected.to be_an_instance_of(Mars::Rover::Transmission) }
      it { expect(transmission).to eq("1 3 N\n4 1 E\n4 2 W") }
    end
  end
end
