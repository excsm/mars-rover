require 'spec_helper'

RSpec.describe Mars::Rover::Instruction do
  let(:parse) { described_class.parse(instruction) }

  context 'with no instruction' do
    let(:instruction) { '' }

    it do
      expect { parse }
        .to raise_error(
          Mars::Rover::InstructionError, /Invalid rover instructions!!!/
        )
    end
  end

  context 'with incomplete instruction set' do
    let(:instruction) { '1 2 N' }

    it do
      expect { parse }
        .to raise_error(
          Mars::Rover::InstructionError, /Invalid rover instructions!!!/
        )
    end
  end

  context 'with invalid instruction set' do
    let(:instruction) { 'Troll time :troll_face:' }

    it do
      expect { parse }
        .to raise_error(
          Mars::Rover::InstructionError, /Invalid rover instructions!!!/
        )
    end
  end

  context 'with invalid initial rover x position' do
    let(:instruction) { "S 2 N\nLMLMLMLMM" }

    it do
      expect { parse }
        .to raise_error(
          Mars::Rover::InstructionError, /Invalid rover instructions!!!/
        )
    end
  end

  context 'with invalid initial rover y position' do
    let(:instruction) { "1 W N\nLMLMLMLMM" }

    it do
      expect { parse }
        .to raise_error(
          Mars::Rover::InstructionError, /Invalid rover instructions!!!/
        )
    end
  end

  context 'with invalid rover command' do
    let(:instruction) { "1 2 N\nLMLMFMLMM" }

    it do
      expect { parse }
        .to raise_error(
          Mars::Rover::InstructionError, /Invalid rover instructions!!!/
        )
    end
  end

  context 'with corrupted rover command' do
    let(:instruction) { "1 2 N\nLMLM MLMM" }

    it do
      expect { parse }
        .to raise_error(
          Mars::Rover::InstructionError, /Invalid rover instructions!!!/
        )
    end
  end

  context 'with correct instruction set' do
    subject { parse }

    let(:instruction) { <<-INSTRUCTION.strip }
        1 2 N
        LMLMLMLMM
      INSTRUCTION

    it { is_expected.to be_an_instance_of(described_class) }

    context 'rover commands size' do
      subject { parse.commands.length }

      it { is_expected.to be(9) }
    end

    context 'initial position' do
      subject { parse.position }

      it { is_expected.to eq([1, 2]) }
    end

    context 'initial orientation' do
      subject { parse.orientation }

      it { is_expected.to eq(Mars::Rover::NORTH) }
    end

    context 'movement commands' do
      subject { parse.commands }

      let(:expected_commands) do
        [
          Mars::Rover::TURN_LEFT,
          Mars::Rover::MOVE_FORWARD,
          Mars::Rover::TURN_LEFT,
          Mars::Rover::MOVE_FORWARD,
          Mars::Rover::TURN_LEFT,
          Mars::Rover::MOVE_FORWARD,
          Mars::Rover::TURN_LEFT,
          Mars::Rover::MOVE_FORWARD,
          Mars::Rover::MOVE_FORWARD
        ]
      end

      it { is_expected.to eq(expected_commands) }
    end
  end
end
