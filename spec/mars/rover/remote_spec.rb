require 'spec_helper'

RSpec.describe Mars::Rover::Remote do
  subject { transmit_command }

  let(:plateau) { Mars::Rover::Plateau.new(5, 5) }
  let(:remote_rover) { described_class.new(plateau, position, orientation) }
  let(:transmit_command) { remote_rover.transmit_command(command) }
  let(:position) { [1, 2] }
  let(:orientation) { Mars::Rover::NORTH }

  describe '#transmit_command' do
    let(:position) { [1, 2] }
    let(:orientation) { Mars::Rover::NORTH }

    context 'when no command' do
      let(:command) { nil }

      it do
        expect { transmit_command }.to raise_error(Mars::Rover::TransmissionError)
      end
    end

    context 'when invalid command' do
      let(:command) { Mars::Rover::NORTH }

      it do
        expect { transmit_command }.to raise_error(Mars::Rover::TransmissionError)
      end
    end

    context 'when next movement is off the plateau' do
      let(:position) { [1, 4] }
      let(:command) { Mars::Rover::MOVE_FORWARD }

      it { is_expected.to be_an_instance_of(Mars::Rover::CommandResult) }
      its(:status) { is_expected.to eq(:not_executed) }
      its(:reason) { is_expected.to eq('out_of_bounds') }
    end

    context 'when next movement would encounter an obstacle (e.g another rover)' do
      let(:position) { [1, 2] }
      let(:command) { Mars::Rover::MOVE_FORWARD }
      let(:other_rover) { described_class.new(plateau, [1, 3], Mars::Rover::EAST) }
      let(:other_coordinates) { Mars::Rover::Coordinates.new([1, 3], Mars::Rover::EAST) }

      before do
        plateau.place_rover(other_rover, other_coordinates)
      end

      it { is_expected.to be_an_instance_of(Mars::Rover::CommandResult) }
      its(:status) { is_expected.to eq(:not_executed) }
      its(:reason) { is_expected.to eq('obstacle_conflict') }
    end

    context 'when turn left command' do
      let(:command) { Mars::Rover::TURN_LEFT }

      it { is_expected.to be_an_instance_of(Mars::Rover::CommandResult) }
      its(:status) { is_expected.to eq(:executed) }
      its(:new_coordinates) { is_expected.to be_an_instance_of(Mars::Rover::Coordinates) }

      context 'the new coordinates' do
        subject { transmit_command.new_coordinates }

        its(:position) { is_expected.to eq([1, 2]) }
        its(:orientation) { is_expected.to eq(Mars::Rover::WEST) }
      end
    end

    context 'when turn right command' do
      let(:command) { Mars::Rover::TURN_RIGHT }

      it { is_expected.to be_an_instance_of(Mars::Rover::CommandResult) }
      its(:status) { is_expected.to eq(:executed) }
      its(:new_coordinates) { is_expected.to be_an_instance_of(Mars::Rover::Coordinates) }

      context 'the new coordinates' do
        subject { transmit_command.new_coordinates }

        its(:position) { is_expected.to eq([1, 2]) }
        its(:orientation) { is_expected.to eq(Mars::Rover::EAST) }
      end
    end

    context 'when next movement is on the plateau and no obstacles' do
      let(:command) { Mars::Rover::MOVE_FORWARD }

      it { is_expected.to be_an_instance_of(Mars::Rover::CommandResult) }
      its(:status) { is_expected.to eq(:executed) }
      its(:new_coordinates) { is_expected.to be_an_instance_of(Mars::Rover::Coordinates) }

      context 'the new coordinates' do
        subject { transmit_command.new_coordinates }

        its(:position) { is_expected.to eq([1, 3]) }
        its(:orientation) { is_expected.to eq(Mars::Rover::NORTH) }
      end
    end
  end
end
