require 'bundler/setup'
require 'mars/rover'
require 'rspec/its'

Dir[Mars::Rover.root.join('spec/support/*.rb')].each { |f| require f }

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
  config.include CliSupport
end
