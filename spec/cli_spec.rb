require 'spec_helper'
require 'open3'

RSpec.describe "MARS ROVER CLI" do
  subject { call_cli }
  let(:cli_command) { 'bundle exec exe/mars-rover' }
  let(:call_cli) { exec_cli("#{cli_command} #{cli_args}", typed_input) }
  let(:cli_args) { '' }
  let(:typed_input) { [''] }

  context 'when no args nor input' do
    let(:cli_args) { '' }
    let(:typed_input) { [''] }
    it { is_expected.to eq('error: Missing rover instructions!!!') }
  end

  context 'when no args and bad input' do
    let(:cli_args) { '' }
    let(:typed_input) { ['foo', ''] }
    it { is_expected.to eq('error: Invalid rover instructions!!! (1st line defines plateau size and consists of two numbers e.g 50 75)') }
  end

  context 'when no args and incomplete input' do
    let(:cli_args) { '' }
    let(:typed_input) { ['5 5', '1 2 N', ''] }
    it { is_expected.to eq('error: Invalid rover instructions!!! (Missing rover commands! Must be one of: L or R or M)') }
  end

  context 'when no args and correct input' do
    let(:cli_args) { '' }
    context 'with one rover' do
      let(:typed_input) { ['5 5', '1 2 N', 'LMLMLMLMM', ''] }
      it { is_expected.to eq('1 3 N') }
    end
    context 'with two rovers' do
      let(:typed_input) do
        ['5 5', '1 2 N', 'LMLMLMLMM', '3 3 E', 'MMRMMRMRRM', '']
      end
      it { is_expected.to eq("1 3 N\n4 1 E") }
    end
    context 'with three rovers' do
      let(:typed_input) do
        [
          '5 5',
          '1 2 N',
          'LMLMLMLMM',
          '3 3 E',
          'MMRMMRMRRM',
          '4 2 W',
          'MMRMRMMRMR',
          ''
        ]
      end
      it { is_expected.to eq("1 3 N\n4 1 E\n4 2 W") }
    end
  end
end
