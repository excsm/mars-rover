require 'mars/rover/version'

module Mars
  # Rover namespace
  module Rover

    NORTH = :north
    SOUTH = :south
    EAST = :east
    WEST = :west

    TURN_LEFT = :turn_left
    TURN_RIGHT = :turn_right
    MOVE_FORWARD = :move_forward

    VALID_COMMANDS = [
      TURN_LEFT,
      TURN_RIGHT,
      MOVE_FORWARD
    ].freeze

    # Error thrown when supplied instructions
    # are invalid or missing.
    InstructionError = Class.new(StandardError)

    # Error thrown when transmission to remote
    # rover fails
    TransmissionError = Class.new(StandardError)

    # TODO: Move to CLI
    def self.root
      Pathname.new(File.expand_path('../../..', __FILE__))
    end
  end
end

require 'mars/rover/transmitter'
