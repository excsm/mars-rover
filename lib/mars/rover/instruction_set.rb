require 'parser'

module Mars
  module Rover

    # Represents complete instruction set to be sent
    # to Mars
    class InstructionSet

      attr_reader :plateau_size_x, :plateau_size_y, :instructions

      def initialize(plateau_size_x, plateau_size_y, instructions)
        @plateau_size_x = plateau_size_x
        @plateau_size_y = plateau_size_y
        @instructions = instructions
      end
    end
  end
end
