require 'mars/rover'

module Mars
  module Rover
    # Handles interaction with the command line interface
    class Cli
      def self.run
        new.run
      end

      # Reads input from STDIN
      # and transmits instruction set to Mars
      def run
        read_input
        transmit
      end

      private

      attr_reader :input

      # Transmit instruction set to Mars
      # @return [Mars::Rover::Transmission]
      def transmit
        transmission = Mars::Rover::Transmitter.new(input.join("\n")).transmit
        puts transmission.print
      rescue => err
        puts "error: #{err.message}"
      end

      # Reads input from STDIN
      # Reads each line into an array
      # and only breaks on an empty line
      #
      # Stores captured lines in the `input` instance var.
      def read_input
        @input = []

        while line = STDIN.gets.strip
          input << line if line.length > 0
          break unless line.length > 0
        end
      end
    end
  end
end
