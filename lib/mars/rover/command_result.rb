module Mars
  module Rover
    # Value object for result of command
    # transmitted to remote rover
    class CommandResult

      attr_reader :status, :reason, :new_coordinates

      def initialize(status, reason, new_coordinates = nil)
        @status = status
        @reason = reason
        @new_coordinates = new_coordinates
      end
    end
  end
end
