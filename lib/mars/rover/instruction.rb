require 'parser'

module Mars
  module Rover

    ROVER_POSITION_RE = /\A(?<position_x>\d+)\s+(?<position_y>\d+)\s+(?<orientation>[NSEW]?)\Z/i
    ROVER_COMMANDS_RE = /\A(?<commands>[LRM]+)\Z/i

    ROVER_COMANDS_LEFT = /\AL\Z/i
    ROVER_COMANDS_RIGHT = /\AR\Z/i
    ROVER_COMANDS_MOVE = /\AM\Z/i

    ROVER_ORIENTATION_NORTH = /\AN\Z/i
    ROVER_ORIENTATION_SOUTH = /\AS\Z/i
    ROVER_ORIENTATION_EAST = /\AE\Z/i
    ROVER_ORIENTATION_WEST = /\AW\Z/i

    # Represents instructions to be sent to Rover
    class Instruction

      attr_reader :position, :orientation, :commands

      # Init a new instance
      #
      # @raise [Mars::Rover::InstructionError] if any parser errors
      # @return [Mars::Rover::Instruction]
      def self.parse(instructions)
        new(instructions)
      end

      def initialize(instructions)
        @instructions = instructions
        parse_instructions
      end

      private

      attr_reader :instructions

      # Parses supplied instructions
      #
      # @raise [Mars::Rover::InstructionError] if any parser errors
      def parse_instructions
        coords, commands = instructions.split("\n")
        raise_missing_coords unless coords
        raise_missing_commands unless commands

        parse_coords(coords.strip)
        parse_commands(commands.strip)
      end

      # Parse out the initial position and orientation
      #
      # After this is run the `position` attribute returns an Array<Integer>
      # with 2 entries (x,y).
      #
      # The `orientation` attribute returns an Integer corresponding to 
      # `Mars::Rover::NORTH/SOUTH/EAST/WEST`
      #
      # @raise [Mars::Rover::InstructionError] if any parser errors
      def parse_coords(coords)
        match_data = ROVER_POSITION_RE.match(coords)
        raise_bad_initial_coords unless match_data
        @position = rover_position(match_data)
        @orientation = rover_orientation(match_data[:orientation])
      end

      # Parse out the movement commands for the rover
      #
      # After this is called, `commands` attribute
      # returns an Array<Integer> corresponding to one of:
      # `Mars::Rover::TURN_LEFT`
      # `Mars::Rover::TURN_RIGHT`
      # `Mars::Rover::MOVE_FORWARD`
      #
      # @param [String]
      # @raise [Mars::Rover::InstructionError] if any parser errors
      def parse_commands(commands)
        match_data = ROVER_COMMANDS_RE.match(commands)
        raise_bad_commands unless match_data
        @commands = match_data[:commands].chars.map { |char| rover_command(char) }
      end

      def rover_position(coords)
        [coords[:position_x].to_i, coords[:position_y].to_i]
      end

      # Map orientation to one
      # of the 4 orientation constants
      #
      # @param [String] 
      # @return [Integer] See `Mars::Rover`
      def rover_orientation(orientation)
        case orientation
        when ROVER_ORIENTATION_NORTH
          Mars::Rover::NORTH
        when ROVER_ORIENTATION_SOUTH
          Mars::Rover::SOUTH
        when ROVER_ORIENTATION_EAST
          Mars::Rover::EAST
        when ROVER_ORIENTATION_WEST
          Mars::Rover::WEST
        end
      end

      # Map character to rover command
      #
      # @return [String]
      # @return [Integer] See `Mars::Rover`
      def rover_command(char)
        case char
        when ROVER_COMANDS_LEFT
          Mars::Rover::TURN_LEFT
        when ROVER_COMANDS_RIGHT
          Mars::Rover::TURN_RIGHT
        when ROVER_COMANDS_MOVE
          Mars::Rover::MOVE_FORWARD
        end
      end

      # Returns list of symbols
      # representing valid commands
      # the rovers accept.
      #
      # @return [Array<String>]
      def valid_command_symbols
        "L or R or M"
      end

      # @raise [Mars::Rover::InstructionError]
      def raise_bad_initial_coords
        raise_error(invalid_error("Bad initial rover coordinates e.g. 3 5 W"))
      end

      # @raise [Mars::Rover::InstructionError]
      def raise_missing_commands
        raise_error(invalid_error("Missing rover commands! Must be one of: #{valid_command_symbols}"))
      end

      # @raise [Mars::Rover::InstructionError]
      def raise_bad_commands
        raise_error(invalid_error("Missing rover commands! Must be one of: #{valid_command_symbols}"))
      end

      # @raise [Mars::Rover::InstructionError]
      def raise_missing_coords
        raise_error(invalid_error('Missing rover coords! e.g. 1 2 S'))
      end

      # Raises an `Mars::Rover::InstructionError`
      #
      # @param [String} Additional error message
      # @raise [Mars::Rover::InstructionError]
      def raise_error(error)
        raise Mars::Rover::InstructionError, error
      end

      # @param [String]
      # @return [String]
      def invalid_error(message = '')
        "Invalid rover instructions!!! (#{message})"
      end
    end
  end
end
