require 'mars/rover/coordinates'
require 'mars/rover/command_result'

module Mars
  module Rover
    # Represents the remote rover
    # on the Martian plateau.
    class Remote

      attr_reader :position, :orientation

      # @raise [Mars::Rover::TransmissionError] if an obstacle prevents placement
      def initialize(plateau, position, orientation)
        @plateau = plateau
        @position = position
        @orientation = orientation
      end

      # Transmit a command to remote rover
      #
      # @param [Symbol]
      # @return [Mars::Rover::CommandResult]
      def transmit_command(command)
        raise_error(:missing_command) unless command
        raise_error(:invalid_command) unless valid_command?(command)
        case command
        when Mars::Rover::TURN_LEFT
          turn_left
        when Mars::Rover::TURN_RIGHT
          turn_right
        when Mars::Rover::MOVE_FORWARD
          move_rover
        end
      end

      # Transmit a bunch of commands to the remote rover
      #
      # @param [Mars::Rover::Instruction]
      # @return [Array<Mars::Rover::CommandResult>]
      def transmit_commands(instruction)
        instruction.commands.map do |command|
          transmit_command(command)
        end
      end

      def coordinates
        Mars::Rover::Coordinates.new(position, orientation)
      end

      private

      attr_reader :plateau

      # Transmit command to move rover
      # one grid point forward
      # @return [Mars::Rover::CommandResult]
      def move_rover
        new_coordinates = move_forward
        plateau.place_rover(self, new_coordinates)
        update_coordinates(new_coordinates)
        successfull_command_result('move_forward')
      rescue Mars::Rover::TransmissionError => err
        unsuccessfull_command_result(err.message)
      end

      # Issues command to re-orient rover 90 degress to the left
      # @return [Mars::Rover::CommandResult]
      def turn_left
        @orientation = reorient_anti_clockwise

        successfull_command_result('turn_left')
      rescue Mars::Rover::TransmissionError => err
        unsuccessfull_command_result(err.message)
      end

      # Issues command to re-orient rover 90 degress to the right
      # @return [Mars::Rover::CommandResult]
      def turn_right
        @orientation = reorient_clockwise

        new_coordinates = Mars::Rover::Coordinates.new(position, orientation)
        successfull_command_result('turn_right')
      rescue Mars::Rover::TransmissionError => err
        unsuccessfull_command_result(err.message)
      end

      # Indicates a successfull command
      #
      # @param command [Symbol]
      # @return [Mars::Rover::CommandResult]
      def successfull_command_result(command)
        Mars::Rover::CommandResult.new(:executed, command.to_s,  coordinates)
      end

      # Indicates a failed command
      #
      # @param reason [String]
      # @return [Mars::Rover::CommandResult]
      def unsuccessfull_command_result(reason)
        Mars::Rover::CommandResult.new(:not_executed, reason,  nil)
      end

      # Returns x coordinate of current position.
      #
      # @return [Integer]
      def x_coord
        position.first
      end

      # Returns y coordinate of current position.
      #
      # @return [Integer]
      def y_coord
        position.last
      end

      # Calculates the new position
      # after moving forward one grid point
      # depending on the orientation
      #
      # @return [Mars::Rover::Coordinates]
      def move_forward
        case orientation
        when Mars::Rover::NORTH
          new_position = [x_coord, y_coord + 1]
        when Mars::Rover::SOUTH
          new_position = [x_coord, y_coord - 1]
        when Mars::Rover::EAST
          new_position = [x_coord + 1, y_coord]
        when Mars::Rover::WEST
          new_position = [x_coord - 1, y_coord]
        end

        Mars::Rover::Coordinates.new(new_position, orientation)
      end

      # Re-orients the rover 90 degrees to the left
      #
      # @return [Symbol]
      def reorient_anti_clockwise
        case orientation
        when Mars::Rover::NORTH
          Mars::Rover::WEST
        when Mars::Rover::SOUTH
          Mars::Rover::EAST
        when Mars::Rover::EAST
          Mars::Rover::NORTH
        when Mars::Rover::WEST
          Mars::Rover::SOUTH
        end
      end

      # Re-orients the rover 90 degrees to the right
      #
      # @return [Symbol]
      def reorient_clockwise
        case orientation
        when Mars::Rover::NORTH
          Mars::Rover::EAST
        when Mars::Rover::SOUTH
          Mars::Rover::WEST
        when Mars::Rover::EAST
          Mars::Rover::SOUTH
        when Mars::Rover::WEST
          Mars::Rover::NORTH
        end
      end

      # Update current coordinates
      #
      # @param [Mars::Rover::Coordinates]
      def update_coordinates(coordinates)
        @position = coordinates.position
        @orientation = coordinates.orientation
      end

      # Determines wether the command is valid
      #
      # @param [Symbol]
      # @return [Boolean]
      def valid_command?(command)
        Mars::Rover::VALID_COMMANDS.include?(command)
      end

      # Raises a transmission error
      #
      # @param message [String]
      #
      # @raise [Mars::Rover::TransmissionError]
      def raise_error(message)
        raise Mars::Rover::TransmissionError, message
      end
    end
  end
end
