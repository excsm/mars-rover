module Mars
  module Rover
    # Value object for a set of coordinates
    class Coordinates

      attr_reader :position, :orientation

      def initialize(position, orientation)
        @position = position
        @orientation = orientation
      end

      def to_hash
        {position_x: position.first, position_y: position.last, orientation: print_orientation}
      end

      private

      def print_orientation
        orientation.to_s.chars.first.upcase
      end
    end
  end
end
