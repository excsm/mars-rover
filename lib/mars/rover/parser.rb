require 'mars/rover/instruction_set'
require 'mars/rover/instruction'

module Mars
  module Rover
    # Handles parsing the instruction set
    class Parser

      VALID_NUMERIC = /\A[-+]?[0-9]*\.?[0-9]+\Z/

      attr_reader :grid_size_x, :grid_size_y, :rover_commands

      # @param instruction_set [String]
      def initialize(instruction_set)
        @instruction_set = instruction_set
        @rover_instructions = []
      end

      # Handles parsing instruction set
      #
      # @return [Mars::Rover::InstructionSet]
      def parse
        validate_missing_instructions
        parse_grid_size
        validate_grid_size
        parse_rover_instructions
        Mars::Rover::InstructionSet.new(grid_size_x.to_i, grid_size_y.to_i, rover_instructions)
      end

      private

      attr_reader :instruction_set, :rover_instructions

      # Raises an error if no instructions
      #
      # @raise [Mars::Rover::InstructionError]
      def validate_missing_instructions
        raise_error(missing_error) if missing_instructions?
      end

      # @raise [Mars::Rover::InstructionError] if any errors parsing grid size
      def validate_grid_size
        raise_error invalid_error('1st line defines plateau size and consists of two numbers e.g 50 75') unless grid_size_valid?
      end

      # @return [Boolean]
      def missing_instructions?
        !instruction_set || instruction_set.strip.empty?
      end

      # @return [Boolean]
      def grid_size_valid?
        grid_size_x =~ VALID_NUMERIC && grid_size_y =~ VALID_NUMERIC
      end

      # Raises an error if no instructions
      #
      # @raise [Mars::Rover::InstructionError]
      def parse_grid_size
        coords = instruction_set.split("\n").first.strip
        @grid_size_x, @grid_size_y = coords.split(/\s/)
      rescue
        raise Mars::Rover::InstructionError, invalid_error('Failed to parse plateau size')
      end

      # Raises an error if no instructions
      #
      # @raise [Mars::Rover::InstructionError]
      def parse_rover_instructions
        lines = instruction_set.split("\n")
        lines.shift
        instructions = lines.each_slice(2).map do |line|
          line.join("\n")
        end
        instructions.map do |instruction|
          rover_instructions << Mars::Rover::Instruction.parse(instruction)
        end
      rescue Mars::Rover::InstructionError => err
        raise err
      rescue
        raise Mars::Rover::InstructionError, invalid_error('Failed to parse plateau size')
      end

      # Raises an `Mars::Rover::InstructionError`
      #
      # @param [String} Additional error message
      # @raise [Mars::Rover::InstructionError]
      def raise_error(error)
        raise Mars::Rover::InstructionError, error
      end

      # @return [String]
      def missing_error
        'Missing rover instructions!!!'
      end

      # @param [String]
      # @return [String]
      def invalid_error(message = '')
        "Invalid rover instructions!!! (#{message})"
      end
    end
  end
end
