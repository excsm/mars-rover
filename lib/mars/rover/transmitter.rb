require 'mars/rover/parser'
require 'mars/rover/plateau'
require 'mars/rover/remote'
require 'mars/rover/transmission'

module Mars
  module Rover
    # Handles coordinating the transmission of the instruction set to the
    # rovers on the Martian plateau.
    class Transmitter

      # @param instruction_set [String]
      # @raise [Mars::Rover::InstructionError]
      def initialize(instruction_set)
        @instruction_set = Parser.new(instruction_set).parse
        initialize_plateau
      end

      # @return [Mars::Rover::Transmission]
      def transmit
        new_coordinates = transmit_all_rover_commands
        Mars::Rover::Transmission.new(new_coordinates)
      end

      private

      attr_reader :instruction_set, :transmission, :plateau, :rovers

      def initialize_plateau
        plateau_size_x = instruction_set.plateau_size_x
        plateau_size_y = instruction_set.plateau_size_y
        @plateau = Mars::Rover::Plateau.new(plateau_size_x, plateau_size_y)
      end

      # @return [Array<Mars::Rover::Coordinates]
      def transmit_all_rover_commands
        instruction_set.instructions.map do |instruction|
          rover = init_rover(instruction)
          rover.transmit_commands(instruction)
          rover.coordinates
        end
      end

      # Initialize a remote rover using the
      # position and orientation from the instruction.
      #
      # @return [Mars::Rover::Remote]
      def init_rover(instruction)
        Mars::Rover::Remote.new(
          plateau,
          instruction.position,
          instruction.orientation
        )
      end
    end
  end
end
