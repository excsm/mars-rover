module Mars
  module Rover
    # Value object handling transmission of new coordinates
    # from remote rovers on the Martian plateau.
    class Transmission

      TEMPLATE = "%{position_x} %{position_y} %{orientation}"

      def initialize(new_coordinates)
        @coordinates = new_coordinates
      end

      # @return [String]
      def print
        coordinates.map do |coord|
          TEMPLATE % coord.to_hash
        end.join("\n")
      end

      private

      attr_reader :coordinates
    end
  end
end
