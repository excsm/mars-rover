require 'mars/rover/parser'
require 'mars/rover/coordinates'

module Mars
  module Rover
    # Represents the Martian plateau
    # and is assumed to be a grid.
    class Plateau

      BOTTOM_LEFT_X = 0
      BOTTOM_LEFT_Y = 0

      def initialize(top_right_x, top_right_y)
        @top_right_x = top_right_x
        @top_right_y = top_right_y
        @rovers = {}
      end

      # @raise [Mars::Rover::TransmissionError] if an obstacle prevents placement
      def place_rover(rover, coordinates)
        return unless rover && coordinates
        raise_error(:out_of_bounds) unless within_bounds?(coordinates)
        raise_error(:obstacle_conflict) if conflict?(coordinates)
        rovers[rover] = coordinates
      end

      # Returns current position of
      # Rover
      #
      # @param [Maybe<Mars::Rover::Coordinates>]
      def rover_position(rover)
        rovers[rover]
      end

      private

      attr_reader :top_right_x, :top_right_y, :rovers

      def bottom_left_x
        BOTTOM_LEFT_X
      end

      def bottom_left_y
        BOTTOM_LEFT_Y
      end

      def within_bounds?(coordinates)
        x_coord, y_coord = coordinates.position
        x_within_bounds?(x_coord) && y_within_bounds?(y_coord)
      end

      def conflict?(coordinates)
        rovers.keys.any? { |rover| rover.position == coordinates.position }
      end

      def raise_error(message)
        raise Mars::Rover::TransmissionError, message
      end

      def x_within_bounds?(x_coord)
        x_coord >= bottom_left_x && x_coord <= (top_right_x - 1)
      end

      def y_within_bounds?(y_coord)
        y_coord >= bottom_left_y && y_coord <= (top_right_y - 1)
      end
    end
  end
end
