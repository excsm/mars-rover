## Solution to Mars Rover coding challenge for Xing hiring
## By Saimon Moore

### The coding problem

#### Description

A squad of robotic rovers are to be landed by NASA on a plateau on
Mars. This plateau, which is curiously rectangular, must be navigated
by the rovers so that their on-board cameras can get a complete
view of the surrounding terrain to send back to Earth.
A rover’s position and location is represented by a combination of x
and y co- ordinates and a letter representing one of the four cardinal
compass points. The plateau is divided up into a grid to simplify
navigation. An example position might be 0, 0, N, which means the
rover is in the bottom left corner and facing North.
In order to control a rover, NASA sends a simple string of letters. The
possible letters are ‘L’, ‘R’ and ‘M’. ‘L’ and ‘R’ makes the rover spin
90 degrees left or right respectively, without moving from its current
spot. ‘M’ means move forward one grid point, and maintain the same
Heading.
 
Assume that the square directly North from (x, y) is (x, y+1).


#### INPUT:

The first line of input is the upper-right coordinates of the plateau, the
lower- left coordinates are assumed to be 0,0.
The rest of the input is information pertaining to the rovers that have
been deployed. Each rover has two lines of input. The first line gives
the rover’s position, and the second line is a series of instructions
telling the rover how to explore the plateau.
The position is made up of two integers and a letter separated by
spaces, corresponding to the x and y co-ordinates and the rover’s
orientation.
Each rover will be finished sequentially, which means that the
second rover won’t start to move until the first one has finished
Moving.

#### OUTPUT
The output for each rover should be its final co-ordinates and
heading.

#### Example INPUTs AND OUTPUTs

    **Test Input**:

    5 5
    1 2 N
    LMLMLMLMM
    3 3 E
    MMRMMRMRRM

    **Expected Output**:

    1 3 N
    5 1 E

#### Expectations

"In terms of how you approach the problem and what exactly you deliver - that's *entirely* up to you."


### My solution

#### Exploration

Initial sketches and concepts:

![sketch](./mars-rover-sketch.jpg)
![concepts](./mars-rover-concepts.jpg)

#### Usage

You can communicate with the remote martian rovers via a simple command line executable
`mars-rover`.

When executed, the cli program will allow you to send the rovers instructions and receive their transmission of their final coordinates.

In order to execute the mars-rover command line executable you need the following installed in your system:

- ruby (>= 2.3) 
- bundler (>= 1.14)
- an internet connection

##### Setup

    git clone git@bitbucket.org:excsm/mars-rover.git
    cd mars-rover
    bundle
    
##### Executing the cli

To run via bundler:

    bundle exec exe/mars-rover

To run via gem: 

    gem build mars-rover.gemspec
    gem install mars-rover-x.x.x.gem
    mars-rover

##### Executing the specs

You can run the whole suite of tests via:

    bundle exec rspec spec


##### User interaction

You can then introduce the rover instructions as described in the *"test input"*
section above, *pressing return after each line*.

Enter an *empty newline* to "transmit" the "instructions" to the rover, they will 
immediately respond with either a descriptive error message or the final
coordinates.

**WARNING**: If your movement commands are badly programmed, you may not receive the
final coordinates you expect. The rover will not execute any commands that cause
it to go off piste (out of bounds) or where an obstacle (e.g. another rover)
blocks it's way.
